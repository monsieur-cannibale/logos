#!/bin/bash

srindex=$1
location=$2
lamedbdir=$3
type=${srindex##*/}
type=${type%.*}

if [ -d "$location" ]; then
    rm -rf $location
fi
mkdir -p $location

cd $location

cat $srindex | grep -v '\-\-\-' | grep -v '^$' | while read line ; do
    if echo "$line" | grep -q '^# '; then
        logoname=${line#??}
        firstchar=`echo $logoname | grep -o '^.'`
        fullfilepath=$type/$firstchar/$logoname.png
    fi

    if echo "$line" | grep -q '.*_.*_.*_.*'; then

        serviceref=`echo $line | tr [A-Z] [a-z]`
        serviceref=(${serviceref//_/ })
        lamedbref=`echo ${serviceref[0]} | sed -e :a -e 's/^.\{1,3\}$/0&/;ta'`":"`echo ${serviceref[3]} | sed -e :a -e 's/^.\{1,7\}$/0&/;ta'`":"`echo ${serviceref[1]} | sed -e :a -e 's/^.\{1,3\}$/0&/;ta'`":"`echo ${serviceref[2]} | sed -e :a -e 's/^.\{1,3\}$/0&/;ta'`

        lamedbref=$(cat $lamedbdir/* | grep $lamedbref)
        lamedbref=(${lamedbref//:/ })
        servicetype=`printf "%x\n" ${lamedbref[4]} | tr [a-z] [A-Z]`

        case $type in
            "tv")
                ln -s $fullfilepath "1_0_"$servicetype"_"$line"_0_0_0.png"
                if [ $servicetype != "1" ]; then
                    ln -s $fullfilepath "1_0_1_"$line"_0_0_0.png"
                fi
                ;;
            "radio")
                ln -s $fullfilepath "1_0_"$servicetype"_"$line"_0_0_0.png"
                if [ $servicetype != "2" ]; then
                    ln -s $fullfilepath "1_0_2_"$line"_0_0_0.png"
                fi
                ;;
        esac
    fi
done
